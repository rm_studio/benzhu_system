"""
Django settings for benzhu_system project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_NAME = os.path.basename(BASE_DIR)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'u@$mtb^%+oa=r#^%rmiu0o_pw&4w0svv^pb86&d)f5!(rq$dod'
SITE_ID = 1

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.comments',
    'django.contrib.sites',
    'mptt',
    'ckeditor',
    'imagekit',
    'PIL',
    'benzhu_system.core.taxonomy',
    'benzhu_system.core.contact',
    'benzhu_system.core.atom',
    'benzhu_system.core.gallery',
    'benzhu_system.core.page',
    'benzhu_system.apps.comment',
    'benzhu_system.apps.atom.article',
    'benzhu_system.apps.atom.unit',
    'benzhu_system.apps.atom.product',
    'benzhu_system.apps.service',
    'benzhu_system.apps.community',
    'benzhu_system.apps.account',
)

COMMENTS_APP = 'benzhu_system.apps.comment'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

#AUTH_PROFILE_MODULE = 'benzhu_system.apps.account.Profile'

ROOT_URLCONF = PROJECT_NAME + '.urls'

WSGI_APPLICATION = PROJECT_NAME + '.wsgi.application'


# User for @login_require
LOGIN_URL = '/user_login'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'bz_db',
        'USER': 'benzhu',
        'PASSWORD': '3qJ89PaDju2uwyTE',
        'HOST': 'localhost',
        'PORT': '3306'
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

# locale directory MUST BE 'zh_CN'
LANGUAGE_CODE = 'zh-cn'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = True


########################### Directories & URLs ############################

# Project path.
PROJECT_DIR = os.path.join(BASE_DIR, PROJECT_NAME)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_ROOT = os.path.join(PROJECT_DIR, "static")
STATIC_URL = '/static/'

# Staticfiles directory IS NOT THE SAME AS Static directory
# Copy the statics files to staticfiles directory and empty the Static directory.
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_DIR, 'staticfiles'),
)

# Templates directory.
TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, 'templates'),
)

# For Medias and django-ckeditor
MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(PROJECT_DIR, "media")

CKEDITOR_UPLOAD_PATH = "node_images"
CKEDITOR_JQUERY_URL = '/static/admin/js/libs/jquery/2.1.1/jquery.min.js'

# Upload & Thumbnail
UPLOAD_RELATE_ROOT = 'upload_items/'
THUMB_RELATE_ROOT = 'upload_items/thumb'
THUMB_URL = 'upload_items/thumb/'
THUMB_ROOT = os.path.join(MEDIA_ROOT, 'upload_items/thumb')

# locale
LOCALE_PATHS = (
    os.path.join(PROJECT_DIR, "conf/locale"),
)

###########################################################################


# CKEditor configs.

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': (
        ['div', 'Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'],
        ['Cut', 'Copy', 'Paste', 'PasteText',  'PasteFromWord',  '-',  'Print',  'SpellChecker',  'Scayt'],
        ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
        ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button',  'ImageButton', 'HiddenField'],
        ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink', 'Anchor'],
        ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
        ['Styles', 'Format', 'Font', 'FontSize'],
        ['TextColor', 'BGColor'],
        ['Maximize', 'ShowBlocks', '-', 'About',  'pbckcode'],
        ),
    }
}
