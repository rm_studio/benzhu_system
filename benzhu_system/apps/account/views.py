from django.template import loader, Context
from django.http import Http404, HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User  
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import authenticate, login as user_login, logout as user_logout
from django.utils.translation import ugettext_lazy as _
from benzhu_system.apps.atom.unit.models import Community
from benzhu_system.apps.atom.article.models import Feedback
from benzhu_system.apps.account.models import Profile
from benzhu_system.apps.atom.product.models import Service
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.contrib.auth.hashers import make_password
from django.contrib.auth import logout


def login(request):

    url_next = request.GET.get('next')
    try:
        str_next = url_next.replace('/user_center/?community_id=', '')
        print('get:*****'+request.GET.get('next')+'*****')
        print('str_next:*****'+str_next+'*****')
        community_id = int(str_next)
        request.session['community_id'] = str(community_id)
    except:
        community_id = request.session.get('community_id')

    #print('c_id:*****'+community_id+'*****')
    community = Community.objects.get(id=community_id)

    errors = []
    account = None
    password = None  
    if request.method == 'POST':
        if not request.POST.get('account'):  
            errors.append(_('Please Enter account'))  
        else:
            account = request.POST.get('account')

        if not request.POST.get('password'):  
            errors.append(_('Please Enter password'))  
        else:
            password = request.POST.get('password')  

        if account is not None and password is not None:
            user = authenticate(username=account, password=password)
            if user is not None:
                if user.is_active:
                    user_login(request, user)
                    return HttpResponseRedirect(request.GET.get('next') and request.GET.get('next') or '/')
                else:
                    errors.append(_('disabled account'))
            else:
                errors.append(_('invaild user'))

    return render_to_response('login.html', {'community': community, 'errors': errors}, context_instance=RequestContext(request))


def register(request):
    community = Community.objects.get(id=request.session.get('community_id'))
    errors = []  
    account = None  
    password = None  
    password_re = None  
    email = None  
    compare_flag = False
  
    if request.method == 'POST':  
        if not request.POST.get('account'):  
            errors.append(_('Please Enter account'))  
        else:  
            account = request.POST.get('account') 

        if not request.POST.get('password'):  
            errors.append(_('Please Enter password'))
        else:  
            password = request.POST.get('password')

        if not request.POST.get('password2'):  
            errors.append(_('Please Enter retype password'))  
        else:  
            password_re = request.POST.get('password2')
            
        if not request.POST.get('email'):  
            email = request.POST.get('account') + "@bz088.com"
        else:  
            email = request.POST.get('email')

        if not request.POST.get('truename'):
            true_name = _('Guest')
        else:
            true_name = request.POST.get('truename')

        tel = request.POST.get('tel')

        community_address = request.POST.get('community_address')

        if password is not None and password_re is not None:  
            if password == password_re:  
                compare_flag = True
            else:
                errors.append(_('password2 is diff password'))  
  
        if account is not None and password is not None and password_re is not None and email is not None and compare_flag:
            user = User.objects.create_user(account,email,password)  
            user.is_active = True
            user.save

            profile = Profile(
                user=user,
                true_name=true_name,
                tel=tel,
                community=Community.objects.get(id=request.session.get('community_id')),
                community_address=community_address,
            )
            profile.save()

            return HttpResponseRedirect('/user_login')  
  
    return render_to_response('register.html', {'community': community, 'errors': errors}, context_instance=RequestContext(request))

  
def logout(request):
    community_id = request.session.get('community_id')
    user_logout(request)
    request.session['community_id'] = community_id
    print(request.session.get('community_id'))
    return HttpResponseRedirect('/')

@login_required
def user_center(request):

    community_id = request.GET.get("community_id")
    if community_id is not None:
        request.session['community_id'] = community_id

    me = request.user
    profile = Profile.objects.get(user=me)

    try:
        t = loader.get_template("user_center.html")
    except IndexError:
        raise Http404()

    c = Context({"user": me, "profile": profile})
    return HttpResponse(t.render(c))


def user_settings(request):
    try:
        t = loader.get_template("user_settings.html")
    except IndexError:
        raise Http404()

    c = Context()
    return HttpResponse(t.render(c))


def myfeedback(request):

    me = request.user
    profile = Profile.objects.get(user=me)
    community = profile.community

    if request.method == 'POST':

        try:
            info = request.POST.get('info')
        except ValueError:
            raise Http404()

        feed_back = Feedback()
        feed_back.name = request.user.pk
        feed_back.info = info
        feed_back.time = timezone.now()
        feed_back.community = community
        feed_back.save()
        feed_back.person.add(request.user)
        feed_back.save()

        feed_backs = Feedback.objects.filter(person=request.user.pk).order_by("-time")

        return render_to_response('feedback.html', {"community": community, "feed_backs": feed_backs},
                                  context_instance=RequestContext(request))

    feed_backs = Feedback.objects.filter(person=request.user.pk).order_by("-time")

    try:
        t = loader.get_template("feedback.html")
    except IndexError:
        raise Http404()

    c = Context({"community": community, "feed_backs": feed_backs})
    return render_to_response('feedback.html', {"community": community, "feed_backs": feed_backs},
                              context_instance=RequestContext(request))


def myfeedback_detail(request, offset):

    me = request.user
    profile = Profile.objects.get(user=me)
    community = profile.community

    try:
        feedback_id = int(offset)
    except ValueError:
        raise Http404()

    feed_back = Feedback.objects.get(id=feedback_id)

    try:
        t = loader.get_template("feedback_detail.html")
    except IndexError:
        raise Http404()

    c = Context({"community": community, "feedback": feed_back})
    return HttpResponse(t.render(c))


@login_required
def myservice(request):

    community_id = request.GET.get("community_id")
    if community_id is not None:
        request.session['community_id'] = community_id

    if request.method == 'POST':

        try:
            key = request.POST.get('key')
        except ValueError:
            raise Http404()

        posts = Service.objects.filter(Q(name__contains=key) | Q(info__contains=key))

        return render_to_response('service_list.html', {"posts": posts}, context_instance=RequestContext(request))

    me = request.user
    profile = Profile.objects.get(user=me)
    services = profile.service.all

    return render_to_response('user_favourite_service.html', {"services": services},
                              context_instance=RequestContext(request))


def user_info(request):

    me = request.user
    profile = Profile.objects.get(user=me)

    if request.method == 'POST':

        try:
            true_name = request.POST.get('true_name')
            tel = request.POST.get('tel')
            community_address = request.POST.get('community_address')
        except ValueError:
            raise Http404()

        profile.true_name = true_name
        profile.tel = tel
        profile.community_address = community_address
        profile.save()

        return render_to_response('user_settings.html', {"profile": profile}, context_instance=RequestContext(request))

    return render_to_response('user_info.html', {"profile": profile}, context_instance=RequestContext(request))


def get_user_info(request):
    me = User.objects.get(pk=request.GET.get('pk'))
    profile = Profile.objects.get(user=me)
    
    return HttpResponse(profile.true_name)


def user_password(request):

    me = request.user

    if request.method == 'POST':

        try:
            password = request.POST.get('password')
        except ValueError:
            raise Http404()

        me.password = make_password(password)
        me.save()

        logout(request)

        return HttpResponseRedirect('/user_login')

    return render_to_response('user_password.html', {"me": me}, context_instance=RequestContext(request))
