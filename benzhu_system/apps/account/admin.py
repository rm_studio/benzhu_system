from django import forms
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from benzhu_system.apps.account.models import Profile
from benzhu_system.apps.atom.product.models import Service
from django.db import models


class ProfileInline(admin.StackedInline):
    model = Profile
    # fk_name = 'user'
    max_num = 1
    can_delete = False
    filter_horizontal = ["service", ]


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class CustomUserAdmin(UserAdmin):
    form = UserChangeForm
    inlines = [ProfileInline, ]

    # rewrite querysets of useradmin
    def get_queryset(self, request):
        
        # check permission function must be has_perm(app_name.permission_name)
        if ( not request.user.is_superuser ) and request.user.has_perm('account.rin'):
            queryset = User.objects.filter(username__contains='rin')
        else:
            queryset = super(CustomUserAdmin, self).get_queryset(request)

        return queryset
    

admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
