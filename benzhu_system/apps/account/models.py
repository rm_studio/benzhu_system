from django.db import models
from django.contrib.auth.models import User
from benzhu_system.apps.atom.unit.models import Community
from benzhu_system.apps.atom.product.models import Service
from django.utils.translation import ugettext_lazy as _


class Profile(models.Model):
    user = models.ForeignKey(User, unique=True)
    true_name = models.CharField(_('True Name'), max_length=10, default='',  null=True, blank=True)
    tel = models.CharField(_('Telephone'), max_length=150, default='',  null=True, blank=True)
    community = models.ForeignKey(Community, default='',  null=True, blank=True)
    community_address = models.CharField(_('Community Address'), max_length=150,  null=True, blank=True)
    service = models.ManyToManyField(Service,  null=True, blank=True)

    def __unicode__(self):
        return self.user.username

    class Meta:
        db_table = 'account_profile'
        verbose_name = _('User')
        verbose_name_plural = _(u'User Profile')
        permissions = (
            ("rin", _("Only display 'rin' user permission")),
        )


def create_user_profile(sender, instance, created, **kwargs):
    """Create the UserProfile when a new User is saved"""
    if created:
        profile = Profile()
        profile.user = instance
        profile.save()

