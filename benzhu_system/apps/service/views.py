from django.template import loader, Context
from django.http import Http404, HttpResponse, HttpResponseRedirect
from benzhu_system.apps.atom.product.models import Service
from benzhu_system.apps.atom.unit.models import Company
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.db.models import Q, Avg
from django.contrib.auth.decorators import login_required
from benzhu_system.apps.account.models import Profile
from benzhu_system.core.gallery.models import GalleryManagement
from benzhu_system.apps.comment.models import StarComment
from django.contrib.contenttypes.models import ContentType


def service(request):

    community_id = request.GET.get("community_id")
    if community_id is not None:
        request.session['community_id'] = community_id

    if request.method == 'POST':

        try:
            key = request.POST.get('key')
        except ValueError:
            raise Http404()

        posts = Service.objects.filter(Q(name__contains=key) | Q(info__contains=key))

        return render_to_response('service_list.html', {"posts": posts}, context_instance=RequestContext(request))

    gallery = GalleryManagement.objects.get(id=1)
    gallery_pic = gallery.galleryPic.all

    t = loader.get_template("service.html")
    c = Context()
    return render_to_response('service.html', {"gallery": gallery_pic}, context_instance=RequestContext(request))


def service_list(request, offset):

    if request.method == 'POST':

        try:
            key = request.POST.get('key')
        except ValueError:
            raise Http404()

        posts = Service.objects.filter(Q(name__contains=key) | Q(info__contains=key))

        return render_to_response('service_list.html', {"posts": posts}, context_instance=RequestContext(request))

    try:
        type_id = int(offset)
    except ValueError:
        raise Http404()
    
    posts = Service.objects.filter(type=type_id)
    stars = {}
    for post in posts:
        try:
            content_type = ContentType.objects.get(name='Service') 
            star_comment = StarComment.objects.filter( 
                    Q( object_pk = post.pk ) & 
                    Q( content_type = content_type.id ) 
            )
            stars[post.pk] = star_comment.aggregate(Avg('star')).get('star__avg')
        except:
            stars[post.pk] = 0

    return render_to_response('service_list.html', {"posts": posts, "stars": stars}, context_instance=RequestContext(request))


def service_detail(request, offset):
    try:
        service_id = int(offset)
    except ValueError:
        raise Http404()
    
    posts = Service.objects.filter(id=service_id)
    service = Service.objects.get(id=service_id)
    gallery = service.gallery.galleryPic.all()

    try:
        content_type = ContentType.objects.get(name='Service') 
        star_comment = StarComment.objects.filter( 
                Q( object_pk = service.pk ) & 
                Q( content_type = content_type.id ) 
        )
        avg_star = star_comment.aggregate(Avg('star')).get('star__avg')
    except:
        avg_star = 0

    return render_to_response("service_detail.html", 
            {"posts": posts, "service": service, "ID": service_id, "gallery": gallery, "avg_star": avg_star, },
            context_instance=RequestContext(request))


def company_detail(request, offset):
    try:
        company_id = int(offset)
    except ValueError:
        raise Http404()

    company = Company.objects.get(id=company_id)

    try:
        t = loader.get_template("company_detail.html")
    except IndexError:
        raise Http404()

    c = Context({"company": company})
    return HttpResponse(t.render(c))


@login_required
def service_comment(request, offset):
    try:
        service_id = int(offset)
    except ValueError:
        raise Http404()

    service = Service.objects.get(id=service_id)

    return render_to_response("service_comment.html", {"service": service}, 
        context_instance=RequestContext(request))


@login_required
def service_favourite(request):
    try:
        service_id = request.GET.get("service_id")
    except ValueError:
        raise Http404()

    try:
        remove_sign = int(request.GET.get("remove"))
    except TypeError:
        remove_sign = None

    service = Service.objects.get(id=service_id)

    try:
        profile = Profile.objects.get(user=request.user)
        
        if not remove_sign == 1:
            profile.service.add(service)
        else:
            profile.service.remove(service)

        profile.save()

    except Profile.DoesNotExist:
        raise Http404()

    return HttpResponseRedirect("/service/")

