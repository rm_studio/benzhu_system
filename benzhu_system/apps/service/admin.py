from benzhu_system.apps.atom.product.models import ServiceType, Service
from django.contrib import admin


class ServiceTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'type', 'time', 'state')
    filter_horizontal = ('person',)


admin.site.register(ServiceType, ServiceTypeAdmin)
admin.site.register(Service, ServiceAdmin)