from django.template import loader, Context, RequestContext
from django.http import Http404, HttpResponse
from benzhu_system.apps.atom.unit.models import Community, Management
from benzhu_system.apps.atom.article.models import Notice, Feedback
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.utils import timezone
from django.contrib.auth.decorators import login_required


def community_index(request):

    community_id = request.GET.get("community_id")
    if community_id is not None:
        request.session['community_id'] = community_id
    community_id = request.session.get('community_id')

    print('*****'+community_id+'*****')

    print('session:*******'+request.session.get('community_id')+'*****')

    notices = Notice.objects.filter(community=community_id).order_by("-time")
    community = Community.objects.get(id=community_id)

    try:
        t = loader.get_template("community.html")
    except IndexError:
        raise Http404()

    c = Context({"notices": notices, "community": community})
    return HttpResponse(t.render(c))


def notice_detail(request, offset):
    try:
        notice_id = int(offset)
        community_id = request.session.get('community_id')
    except ValueError:
        raise Http404()

    notice = Notice.objects.get(id=notice_id)
    community = Community.objects.get(id=community_id)

    try:
        t = loader.get_template("notice_detail.html")
    except IndexError:
        raise Http404()

    c = Context({"notice": notice, "community": community})
    return HttpResponse(t.render(c))


def community_detail(request):

    community_id = request.GET.get("community_id")
    if community_id is not None:
        request.session['community_id'] = community_id

    try:
        community_id = request.session.get('community_id')
    except ValueError:
        raise Http404()

    community = Community.objects.get(id=community_id)

    management = Management.objects.get(id=community.management.id)

    gallery = community.gallery.galleryPic.all()

    return render_to_response('community_detail.html', {"community": community, "management": management,
                                                        "gallery": gallery}, context_instance=RequestContext(request))
    

@login_required
def feedback(request):

    community_id = request.GET.get("community_id")
    if community_id is not None:
        request.session['community_id'] = community_id

    if request.method == 'POST':

        try:
            info = request.POST.get('info')
            community_id = request.session.get('community_id')

        except ValueError:
            raise Http404()

        feed_back = Feedback()
        feed_back.name = request.user.pk
        feed_back.info = info
        feed_back.time = timezone.now()
        feed_back.community = Community.objects.get(id=community_id)
        feed_back.save()
        feed_back.person.add(request.user)
        feed_back.save()

        community = Community.objects.get(id=community_id)
        feed_backs = Feedback.objects.filter(person=request.user.pk).order_by("-time")

        return render_to_response('feedback.html', {"community": community, "feed_backs": feed_backs},
                                  context_instance=RequestContext(request))

    try:
        community_id = request.session.get('community_id')
    except ValueError:
        raise Http404()

    community = Community.objects.get(id=community_id)

    feed_backs = Feedback.objects.filter(person=request.user.pk).order_by("-time")

    try:
        t = loader.get_template("feedback.html")
    except IndexError:
        raise Http404()

    c = Context({"community": community, "feed_backs": feed_backs})
    return render_to_response('feedback.html', {"community": community, "feed_backs": feed_backs},
                              context_instance=RequestContext(request))


def feedback_detail(request, offset):

    try:
        community_id = request.session.get('community_id')
        feedback_id = int(offset)
    except ValueError:
        raise Http404()

    community = Community.objects.get(id=community_id)
    feed_back = Feedback.objects.get(id=feedback_id)

    try:
        t = loader.get_template("feedback_detail.html")
    except IndexError:
        raise Http404()

    c = Context({"community": community, "feedback": feed_back})
    return HttpResponse(t.render(c))