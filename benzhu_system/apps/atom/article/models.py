from django.db import models
from benzhu_system.core.atom.models import Atom
from benzhu_system.apps.atom.unit.models import Community
from ckeditor.fields import RichTextField
from benzhu_system.core.gallery.models import GalleryUser


class Article(Atom):
    community = models.ForeignKey(Community)

    class Meta:
        abstract = True


#************************** Article **************************
class Notice(Article):
    pass


class Feedback(Article):
    check_time = models.DateTimeField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    start_comment = RichTextField(default='', null=True, blank=True)
    charger = models.CharField(max_length=150, default='', null=True, blank=True)
    finish_time = models.DateTimeField(null=True, blank=True)
    finish_comment = RichTextField(default='', null=True, blank=True)
    gallery_user = models.ForeignKey(GalleryUser, null=True, blank=True)

    def __unicode__(self):
        return self.info
#************************** Article End **************************
