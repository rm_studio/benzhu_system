from django.shortcuts import render
from benzhu_system.apps.atom.article.models import Feedback
from benzhu_system.apps.atom.unit.models import Community
from django.db.models import Q, Count
from django.contrib.admin.views.decorators import staff_member_required
from django.http import Http404, HttpResponse, HttpResponseRedirect


@staff_member_required
def get_unread_message(request):
    return HttpResponse( 
            Feedback.objects.filter( 
                Q(check_time = None) & Q( community = Community.objects.filter( person = request.user )) 
            ).count() 
    )


