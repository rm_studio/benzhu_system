from benzhu_system.apps.atom.article.models import Notice, Feedback
from benzhu_system.apps.atom.unit.models import Company, Community, Management
from django.contrib import admin


class NoticeAdmin(admin.ModelAdmin):
    list_display = ('name', 'time', 'community', 'state')
    filter_horizontal = ('person',)
    
    # rewrite querysets of useradmin
    def get_queryset(self, request):

        # check permission function must be has_perm(app_name.permission_name)
        if ( not request.user.is_superuser ) and request.user.has_perm('unit.community_management'):

            # permission limited user
            self.fields = ('name', 'community', 'info', 'time',)
            
            community = Community.objects.filter( person = request.user )
            queryset = Notice.objects.filter( community = community ) 
        else:
            # superuser here

            # in fact, the next line is useless. 
            self.fields = ('name', 'gallery', 'community', 'info', 'person', 'time', 'state', 'count')

            queryset = super(NoticeAdmin, self).get_queryset(request)

        return queryset    

    # rewrite manytomany queryset
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        
        if ( not request.user.is_superuser ) and request.user.has_perm('unit.community_management'):
            if db_field.name == "community":
                kwargs["queryset"] = Community.objects.filter( person = request.user )

        return super(NoticeAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('id', 'info', 'time', 'check_time', 'community', 'state')
    filter_horizontal = ('person', )
    #readonly_fields = ('info', 'check_time', )

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('info', 'check_time')
        return self.readonly_fields


    # rewrite querysets of useradmin
    def get_queryset(self, request):

        # check permission function must be has_perm(app_name.permission_name)
        if ( not request.user.is_superuser ) and request.user.has_perm('unit.community_management'):

            # permission: limited user 
            community = Community.objects.filter( person = request.user )
            queryset = Feedback.objects.filter( community = community ) 
        else:
            # superuser here
            queryset = super(FeedbackAdmin, self).get_queryset(request)

        return queryset

    # Hook for render_change_form is loaded
    def render_change_form(self, request, context, add, change, form_url, obj):

        if change:
            feedback = context['original']

            if not feedback.check_time:
                from django.utils import timezone
                feedback.check_time = timezone.now()
                feedback.save()
            
        return super(FeedbackAdmin, self).render_change_form(request, context)


# register admins
admin.site.register(Notice, NoticeAdmin)
admin.site.register(Feedback, FeedbackAdmin)
