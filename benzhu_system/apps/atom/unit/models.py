from django.db import models
from benzhu_system.core.atom.models import Atom
from benzhu_system.core.contact.models import Phone, Address
from django.utils.translation import ugettext_lazy as _


class Unit(Atom):
    phone = models.ManyToManyField(Phone, default='', blank=True)
    address = models.ForeignKey(Address, default='', blank=True)

    class Meta:
        abstract = True


#************************** Unit **************************
class Management(Unit):

    def __unicode__(self):
        return self.name


class Community(Unit):
    management = models.ForeignKey(Management)
        
    class Meta:
        permissions = (
            ("community_management", _("Current community limited permission")),
        )

    def __unicode__(self):
        return self.name


class Company(Unit):
    validate = models.CharField(max_length=150, default='',  null=True, blank=True)
    gps = models.CharField(max_length=50, default='',  null=True, blank=True)

    def __unicode__(self):
        return self.name
#************************** Unit End **********************
