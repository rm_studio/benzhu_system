from django.contrib import admin
from benzhu_system.apps.atom.unit.models import Company, Community, Management
from benzhu_system.apps.account.models import Profile
from django.contrib.auth.models import User


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    filter_horizontal = ('person', 'phone',)


class CommunityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    filter_horizontal = ('person', 'phone')

    # rewrite querysets of useradmin
    def get_queryset(self, request):

        # check permission function must be has_perm(app_name.permission_name)
        if ( not request.user.is_superuser ) and request.user.has_perm('unit.community_management'):
            queryset = Community.objects.filter(person=request.user)
            self.fields = ('info',)
        else:
            queryset = super(CommunityAdmin, self).get_queryset(request)

        return queryset
   
    # rewrite manytomany queryset
    def formfield_for_manytomany(self, db_field, request, **kwargs):
        
        if ( not request.user.is_superuser ) and request.user.has_perm('unit.community_management'):
            if db_field.name == "person":
                kwargs["queryset"] = User.objects.filter( pk = request.user.pk )

        return super(CommunityAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


class ManagementAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    filter_horizontal = ('person', 'phone')

    # rewrite querysets of useradmin
    def get_queryset(self, request):

        # check permission function must be has_perm(app_name.permission_name)
        if ( not request.user.is_superuser ) and request.user.has_perm('unit.community_management'):
            queryset = Management.objects.filter(person=request.user)
            self.fields = ('info',)
        else:
            queryset = super(ManagementAdmin, self).get_queryset(request)

        return queryset



admin.site.register(Company, CompanyAdmin)
admin.site.register(Community, CommunityAdmin)
admin.site.register(Management, ManagementAdmin)