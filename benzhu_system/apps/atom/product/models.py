from benzhu_system.core.atom.models import Atom
from benzhu_system.apps.atom.unit.models import Company
from django.db import models


class Product(Atom):
    company = models.ForeignKey(Company)

    class Meta:
        abstract = True


#************************** Product **************************
class ServiceType(models.Model):
    name = models.CharField(max_length=150)

    def __unicode__(self):
        return self.name


class Service(Product):
    type = models.ForeignKey(ServiceType)

    def __unicode__(self):
        return self.name
#************************** Product End **************************
