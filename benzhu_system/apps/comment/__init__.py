from benzhu_system.apps.comment.models import StarComment
from benzhu_system.apps.comment.forms import StarCommentForm

def get_model():
    return StarComment

def get_form():
    return StarCommentForm

default_app_config = 'benzhu_system.apps.comment.apps.AuthConfig'
