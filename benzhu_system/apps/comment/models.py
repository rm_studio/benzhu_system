from django.db import models
from django.contrib.comments.models import Comment
from benzhu_system.core.gallery.models import GalleryUser


class StarComment(Comment):
    star = models.IntegerField(default=0)
    gallery_user = models.ForeignKey(GalleryUser, null=True, blank=True)
