from django.apps import AppConfig

from django.utils.translation import ugettext_lazy as _


class AuthConfig(AppConfig):
    name = 'benzhu_system.apps.comment'
    verbose_name = _("Comments")
