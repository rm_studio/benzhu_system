from django import forms
from django.contrib.comments.forms import CommentForm
from benzhu_system.apps.comment.models import StarComment

class StarCommentForm(CommentForm):
    star = forms.IntegerField()

    def get_comment_model(self):
        # Use our custom comment model instead of the built-in one.
        return StarComment

    def get_comment_create_data(self):
        # Use the data of the superclass, and add in the title field
        data = super(StarCommentForm, self).get_comment_create_data()
        data['star'] = self.cleaned_data['star']
        return data
