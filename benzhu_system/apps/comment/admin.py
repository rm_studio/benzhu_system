from .models import StarComment
from django.contrib import admin
from django.contrib.comments.admin import CommentsAdmin
from django.utils.translation import ugettext_lazy as _, ungettext_lazy


class StarCommentAdmin(CommentsAdmin):
    fieldsets = (
        (None,
            {'fields': ('content_type', 'object_pk', 'site')}
        ),
        (_('Content'),
            {'fields': ('user', 'user_name', 'user_email', 'user_url', 'comment', 'star')}
        ),
        (_('Metadata'),
            {'fields': ('submit_date', 'ip_address', 'is_public', 'is_removed')}
        ),
    )

admin.site.register(StarComment, StarCommentAdmin)
