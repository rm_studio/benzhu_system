from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template import loader, Context


def index(request):
    t = loader.get_template("index.html")
    c = Context()
    return HttpResponse(t.render(c))


