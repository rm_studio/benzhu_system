from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from benzhu_system.apps.service.views import service, service_list, service_detail, company_detail, service_comment, \
    service_favourite
from benzhu_system.apps.community.views import community_index, notice_detail, community_detail, feedback, \
    feedback_detail
from benzhu_system.apps.base.views import index
from benzhu_system.core.page.views import page
from benzhu_system.apps.account.views import login, logout, register, user_center, user_settings, myfeedback, \
    myfeedback_detail, myservice, user_info, user_password, get_user_info
from benzhu_system.apps.comment.views import comment_posted
from benzhu_system.apps.atom.article.views import get_unread_message


urlpatterns = patterns('',

    url(r'^$', index),
    url(r'^page/(\d+)/$', page),

    url(r'^community/$', community_index),
    url(r'^community/notice/(\d+)$', notice_detail),
    url(r'^community/introduction/$', community_detail),
    url(r'^community/feedback/$', feedback),
    url(r'^community/feedback/(\d+)$', feedback_detail),

    url(r'^service/$', service),
    url(r'^service/(\d+)/$', service_list),
    url(r'^service/\d+/(\d+)/$', service_detail),
    url(r'^company/(\d+)/$', company_detail),
    url(r'^service/\d+/(\d+)/comment$', service_comment),
    url(r'^service/favourite$', service_favourite),

    url(r'^user_login/$', login),
    url(r'^user_logout/$', logout),
    url(r'^get_user_info$', get_user_info),
    url(r'^register/$', register),
    url(r'^user_center/$', user_center),
    url(r'^settings/$', user_settings),
    url(r'^myfeedback/$', myfeedback),
    url(r'^myfeedback/(\d+)$', myfeedback_detail),
    url(r'^myservice/$', myservice),
    url(r'^user_info/$', user_info),
    url(r'^user_password/$', user_password),

    url(r'^admin/article/feedback/count', get_unread_message),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^comments/posted/$', comment_posted),
    url(r'^comments/', include('django.contrib.comments.urls')),
)

# Append
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
