from django.db import models
from ckeditor.fields import RichTextField
from mptt.models import MPTTModel


class Vocabulary(models.Model):
    name = models.CharField('Vocabulary Name', max_length = 100, blank = False) 
    slug = models.SlugField(db_index = True)
    description = RichTextField(default='')

    def __unicode__(self):  
        return u'%s' % self.name


class Team(MPTTModel):
    vocabulary = models.ForeignKey(Vocabulary)
    parent = models.ForeignKey('self', null=True, blank=True)
    name = models.CharField('Name', max_length=100, blank=False)
    slug = models.SlugField(db_index=True)
    description = RichTextField(default='')
    weight = models.IntegerField()

    '''
    def __init__(self, *args, **kwargs):  
        super(Team, self).__init__(*args, **kwargs) 
        name = models.CharField('Team Name', max_length = 100, blank = False)
        name.contribute_to_class('name', self)
    '''

    class MPTTMeta:
        parent_attr = 'parent'

    def __unicode__(self):  
        return u'%s' % self.name  
