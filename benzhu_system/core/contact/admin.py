from django.contrib import admin
from benzhu_system.core.contact.models import *


class PhoneAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'number')


class AddressBaseAdmin(admin.ModelAdmin):
    list_display = ('id', 'province', 'city', 'area')


class AddressAdmin(admin.ModelAdmin):
    list_display = ('id', 'address_base', 'address_detail')

admin.site.register(Phone, PhoneAdmin)
admin.site.register(AddressBase, AddressBaseAdmin)
admin.site.register(Address, AddressAdmin)