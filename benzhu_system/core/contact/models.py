from django.db import models


class Phone(models.Model):
    name = models.CharField(max_length=150)
    number = models.CharField(max_length=150)

    def __unicode__(self):
        return u'%s, %s' % (self.name, self.number)


class AddressBase(models.Model):
    province = models.CharField(max_length=150)
    city = models.CharField(max_length=150)
    area = models.CharField(max_length=150)

    def __unicode__(self):
        return u'%s, %s, %s' % (self.province, self.city, self.area)


class Address(models.Model):
    address_base = models.ForeignKey(AddressBase)
    address_detail = models.CharField(max_length=255)

    def __unicode__(self):
        return u'%s, %s' % (self.address_base, self.address_detail)



