from django.template import loader, Context

from django.http import Http404, HttpResponse

from benzhu_system.core.page.models import Page


def page(request, offset):
    try:
        page_id = int(offset)
    except ValueError:
        raise Http404()
        # t = loader.get_template("home.html")

    posts = Page.objects.filter(id=page_id)
    alias = [p.alias for p in posts]

    try:
        t = loader.get_template( alias[0] + ".html" )
    except IOError:
        t = loader.get_template("page.html")
    except IndexError:
        raise Http404()

    c = Context( {"posts": posts, "ID": page_id} )
    return HttpResponse(t.render(c))

