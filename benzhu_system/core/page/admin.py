from django.contrib import admin

from benzhu_system.core.page.models import Page, PageAdmin


admin.site.register(Page, PageAdmin)
