from django.db import models
from django.contrib import admin
from ckeditor.fields import RichTextField

import datetime

class Page(models.Model):
    title = models.CharField( max_length = 150 )
    alias = models.CharField( max_length = 50, unique = True )
    body  = RichTextField( default = '' )
    timestamp = models.DateTimeField( auto_now_add = True )

class PageAdmin(admin.ModelAdmin):
    list_display = ( 'title', 'alias', 'timestamp')
