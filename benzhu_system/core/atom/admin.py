from django.contrib import admin


class AtomAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'time', 'state')
    filter_horizontal = ('person',)
