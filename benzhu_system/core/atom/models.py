from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from benzhu_system.core.gallery.models import GalleryManagement
from datetime import datetime


class Atom(models.Model):
    name = models.CharField(max_length=150)
    gallery = models.ForeignKey(GalleryManagement, null=True, blank=True)
    info = RichTextField(default='',  null=True, blank=True)
    person = models.ManyToManyField(User, default='',  null=True, blank=True)
    time = models.DateTimeField(default=datetime.now)
    state = models.CharField(max_length=150, default='',  null=True, blank=True)
    count = models.IntegerField(default=0,  null=True, blank=True)

    class Meta:
        abstract = True