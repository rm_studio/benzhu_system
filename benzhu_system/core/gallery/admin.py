from django.contrib import admin
from benzhu_system.core.gallery.models import *


class GalleryPicAdmin(admin.ModelAdmin):
    list_display = ('id', 'admin_thumb', 'links')
    admin_thumb = AdminThumbnail(image_field='thumb')
    fields = ('name', 'pic', 'links')


class GalleryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    filter_horizontal = ('galleryPic',)


admin.site.register(GalleryUser, GalleryAdmin)
admin.site.register(GalleryManagement, GalleryAdmin)
admin.site.register(GalleryUserPic, GalleryPicAdmin)
admin.site.register(GalleryManagementPic, GalleryPicAdmin)
