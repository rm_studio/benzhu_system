from __future__ import print_function, division
from benzhu_system.settings import *
from django.db import models
from django.contrib import admin
from imagekit.admin import AdminThumbnail
from PIL import Image
import os


class Thumbnail:

    @staticmethod
    def make(path, size = 200):
        try:
            pb = Image.open(path)
            w, h = pb.size

            if w > size:
                d = w / size
                h = int(h / d)
                pb.thumbnail((size, h), Image.ANTIALIAS)

            return pb

        except IOError:
            print("cannot create thumbnails for ", path)
            return None


class GalleryPic(models.Model):
    name = models.CharField(max_length=150)
    pic = models.ImageField(
        upload_to=UPLOAD_RELATE_ROOT,
        max_length=255
    )
    thumb = models.ImageField(
        max_length=255, blank=True, null=True
    )
    links = models.CharField(max_length=150)

    def save(self):
        super(GalleryPic, self).save()
        
        base, ext = os.path.splitext(os.path.basename(self.pic.path))
        relate_thumbnail = base + '.thumb' + ext
        thumbnail_buffer = Thumbnail.make(os.path.join(MEDIA_ROOT, self.pic.name))

        thumbnail_url = os.path.join(THUMB_URL,  relate_thumbnail)
        thumbnail_path = os.path.join(THUMB_ROOT, relate_thumbnail)

        thumbnail_buffer.save(thumbnail_path)
        
        self.thumb = thumbnail_url
        
        super(GalleryPic, self).save()

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        abstract = True

'''
    @permalink
    def get_absolute_url(self):
        return ( "model", None, 
            {
                "object_id": self.id,
                "thumbnail": self.thumb,
                "original_image": self.pic,
            }
        )
'''


class GalleryManagementPic(GalleryPic):
    pass


class GalleryUserPic(GalleryPic):
    pass


class Gallery(models.Model):
    name = models.CharField(max_length=150)
    
    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        abstract = True


class GalleryManagement(Gallery):
    galleryPic = models.ManyToManyField(GalleryManagementPic)


class GalleryUser(Gallery):
    galleryPic = models.ManyToManyField(GalleryUserPic)
